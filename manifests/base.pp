group { "puppet":
	ensure => "present",
}

# execute 'apt-get update'
exec { 'apt-update':
  command => '/usr/bin/apt-get update',
}

package { "vim":
	require => Exec['apt-update'],
	ensure => installed,
}

package { "git":
	require => Exec['apt-update'],
	ensure => installed,
}

